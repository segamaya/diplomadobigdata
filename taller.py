# -*- coding: utf-8 -*-
"""
Created on Tue Nov  6 19:00:30 2018

@author: grey
"""

import pandas as pd
import numpy as np

datafile="resources/nyc_311_data_subset-2.csv"
data=pd.read_csv(datafile)

data.info()
data['Incident Zip'].unique()

data.head()
data.columns

def fix_zip(input_zip):
    try:
        input_zip = int(float(input_zip))
    except:
        try:
            input_zip = int(input_zip.split('-')[0])
        except:
            return np.NaN
    if input_zip < 10000 or input_zip > 19999:
        return np.NaN
    return str(input_zip)
            
data['Incident Zip'] = data['Incident Zip'].apply(fix_zip)
data['Incident Zip'].unique()

data['Incident Zip'] = data[data['Incident Zip'].notnull()]
data.info()
#df =df.dropna(how="any")

data[data['Incident Zip'].notnull()]

tmp=data['Latitude'].unique()
data['Longitude'].unique()

data2 = data[data['Latitude'].notnull() & data['Longitude'].notnull() & data['Closed Date'].notnull() ]
data2.info()
data2['Longitude'].unique()


data["Borough"].unique()
data[data["Borough"]=="Unspecified"][["Agency","Incident Zip"]]


data[data['Borough']=='Unspecified'].groupby('Agency').count()
nypd_complaints_total = data[data['Agency']=='NYPD']['Borough'].count()
nypd_unspecified = data[(data['Borough']=='Unspecified') & (data['Agency']=="NYPD")]['Borough'].count()

percentage = nypd_unspecified/nypd_complaints_total*100

data = data[data['Borough'] != 'Unspecified']
import datetime
data['Created Date'] = data['Created Date'].apply(lambda x:datetime.datetime.strptime(x,'%m/%d/%Y %I:%M:%S %p'))
data['Closed Date'] = data['Closed Date'].apply(lambda x:datetime.datetime.strptime(x,'%m/%d/%Y %I:%M:%S %p'))


data['processing_time'] =  data['Closed Date'] - data['Created Date']
data['processing_time'].describe()


